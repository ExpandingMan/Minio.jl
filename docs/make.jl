using Minio
using Documenter

makedocs(;
    modules=[Minio],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo=Remotes.GitLab("ExpandingMan", "Minio.jl"),
    sitename="Minio.jl",
    pages=[
        "Home" => "index.md",
    ],
)
